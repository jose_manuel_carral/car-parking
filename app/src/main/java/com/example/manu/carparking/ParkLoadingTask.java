package com.example.manu.carparking;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by manu on 30/10/2016.
 */
public class ParkLoadingTask extends AsyncTask<Void, Integer, String> {
    private Context context;
    ProgressDialog zonesLoadingDialog;

    public ParkLoadingTask(Context c) {
        context = c;

        zonesLoadingDialog = new ProgressDialog(context);
        zonesLoadingDialog.setMessage("Getting parking zones");
        zonesLoadingDialog.setIndeterminate(true);
        zonesLoadingDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        zonesLoadingDialog.setCancelable(true);
    }

    @Override
    protected void onPreExecute() {
        zonesLoadingDialog.show();
    }

    @Override
    protected void onProgressUpdate(Integer... progress) {
        zonesLoadingDialog.setIndeterminate(false);
        zonesLoadingDialog.setMax(100);
        zonesLoadingDialog.setProgress(progress[0]);
    }


    @Override
    protected String doInBackground(Void... params) {
        LatLng streetPosition = ParkingsContainer.getStreetPosition();
        String latitude = String.valueOf(streetPosition.latitude);
        String longitude = String.valueOf(streetPosition.longitude);

        String zonesRaw = new WebServer().getParkingsByPoint(latitude, longitude);

        return zonesRaw;
    }

    @Override
    protected void onPostExecute(String zonesRaw) {
        ParkingsContainer.clearParkings();

        JSONArray zones;
        try {
            if (zonesRaw.contains("html")){
                throw new Exception("Error connecting to server");
            }
            zones = new JSONArray(zonesRaw);
            for (int i = 0; i < zones.length(); i++) {
                JSONObject zone;
                zone = zones.getJSONObject(i);
                String id = zone.getString("id");
                String lon = zone.getString("longitude");
                String lat = zone.getString("latitude");
                String capacity = zone.getString("capacity");
                String available = zone.getString("available");
                String price = zone.getString("price_per_minute");
                String distance = zone.getString("distance_to_destination");

                Parking p = new Parking(id, lat, lon, capacity, available, price, distance);
                ParkingsContainer.addParking(p);
            }
        } catch (Exception e) {
            Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
        } finally {
            zonesLoadingDialog.dismiss();
            Intent intent = new Intent("dataLoaded");
            context.sendBroadcast(intent); //message of finalisation to MapsActivity
        }
    }
}
