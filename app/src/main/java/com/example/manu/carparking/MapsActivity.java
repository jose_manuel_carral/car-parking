package com.example.manu.carparking;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap; // Might be null if Google Play services APK is not available.
    String[] parkingLatitudes;
    String[] parkingLongitudes;
    String[] parkingAvailabilities;
    LatLng streetPosition;
    String userEmail;
    String userPass;

    private boolean logged;

    private Button loginBtn;
    private TextView loginStatus;

    private void login(String email) {
        logged = true;
        loginBtn.setText("Log Out");
        loginStatus.setText(email + " logged in");
    }

    private void logout() {
        logged = false;
        loginBtn.setText("Log In");
        loginStatus.setText("You are not logged in.");
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) { //message received when ParkLoadingTask has finished of loading everything
            String action = intent.getAction();

            if (action.equals("dataLoaded")) {
                parkingLatitudes = ParkingsContainer.getParkingsLatitudes();
                parkingLongitudes = ParkingsContainer.getParkingsLongitudes();
                parkingAvailabilities = ParkingsContainer.getParkingsAvailabilities();
                MapsActivity.this.setUpMap();

            } else if (action.equals("reserveIntent")) {
                String info = intent.getExtras().getString("info");
                final String type = intent.getExtras().getString("type");
                final String price = intent.getExtras().getString("price");
                if (info.equals("OK")) {
                    //Reserva realizada con éxito, hay que mostrar la pantalla del pago
                    Toast.makeText(MapsActivity.this, "Reserve in process", Toast.LENGTH_SHORT).show();

                    final Dialog dialog = new Dialog(context);
                    dialog.setContentView(R.layout.payment_layout);
                    dialog.setTitle("Payment");
                    final EditText cardNumber = (EditText) dialog.findViewById(R.id.cardNumber);
                    final EditText name = (EditText) dialog.findViewById(R.id.cardName);
                    final EditText cvv = (EditText) dialog.findViewById(R.id.cvv);
                    final EditText exp = (EditText) dialog.findViewById(R.id.expDate);

                    final TextView precio = (TextView) dialog.findViewById(R.id.textViewp0);
                    final TextView type_v = (TextView) dialog.findViewById(R.id.paymentType);

                    //Formatear el precio
                    float pr = Float.parseFloat(price);
                    String precio_2d = String.format("%.2f", pr);

                    precio.setText("Total to pay : "+ precio_2d +"$");
                    type_v.setText("Payment type: " +type);
                    exp.setHint("mm/yy");

                    Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonpOK);

                    dialogButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String c_number = cardNumber.getText().toString();
                            String c_name = name.getText().toString();
                            String c_cvv = cvv.getText().toString();
                            String c_date = exp.getText().toString();

                            c_number = c_number.replaceAll(" ", "").replaceAll("-", "");
                            if(valid_name(c_name)) {
                                if(valid_cvv(c_cvv)){
                                    if(valid_exp(c_date)) {
                                        if (valid_card(c_number)) {
                                            if (valid_type(c_number, type)) {
                                                PaymentTask pt = new PaymentTask(MapsActivity.this, userEmail, userPass, c_number, price,c_name,c_cvv,c_date);
                                                pt.execute(); //Payment - background
                                                dialog.dismiss();
                                            } else {
                                                Toast.makeText(MapsActivity.this, "The payment method is not " + type + "!", Toast.LENGTH_LONG).show();
                                            }
                                        } else {
                                            Toast.makeText(MapsActivity.this, "Invalid credit card number!", Toast.LENGTH_LONG).show();
                                        }
                                    }else {
                                        Toast.makeText(MapsActivity.this, "Please provide a valid expiration date!", Toast.LENGTH_LONG).show();
                                    }
                                } else {
                                    Toast.makeText(MapsActivity.this, "Please provide a valid CVV!", Toast.LENGTH_LONG).show();
                                }
                            } else {
                                Toast.makeText(MapsActivity.this, "Please provide the Card Name!", Toast.LENGTH_LONG).show();
                            }
                        }
                    });

                    Button dialogButton2 = (Button) dialog.findViewById(R.id.btnpCancel);
                    dialogButton2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Toast.makeText(MapsActivity.this, "The reserve was cancelled!", Toast.LENGTH_LONG).show();
                            dialog.cancel();
                        }
                    });
                    dialog.show();

                } else {
                    Toast.makeText(MapsActivity.this, "Can't reserve, please try again!", Toast.LENGTH_SHORT).show();
                }
            } else if (action.equals("loggedIN")) {
                userEmail = intent.getExtras().getString("userEmail");
                userPass = intent.getExtras().getString("userPass");
                login(userEmail);

            } else if (action.equals("paymentIntent")) {

                String info = intent.getExtras().getString("info");

                if (info.equals("OK")) {
                    //Pago realizado con éxito
                    Toast.makeText(MapsActivity.this, "The payment was processed!", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MapsActivity.this, info, Toast.LENGTH_SHORT).show();

                }
            }
        }
    };

    private Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        IntentFilter intentFilter = new IntentFilter("dataLoaded");
        registerReceiver(receiver, intentFilter);

        IntentFilter intentFilter2 = new IntentFilter("reserveIntent");
        registerReceiver(receiver, intentFilter2);

        IntentFilter intentFilter3 = new IntentFilter("loggedIN");
        registerReceiver(receiver, intentFilter3);

        IntentFilter intentFilter4 = new IntentFilter("paymentIntent");
        registerReceiver(receiver, intentFilter4);

        if (ParkingsContainer.getStreetPosition() != null) { //we need to reload everything in case of screen rotation
            streetPosition = ParkingsContainer.getStreetPosition();
        }

        if (ParkingsContainer.parkingsNumber() > 0) {//we need to reload everything this in case of screen rotation
            parkingLongitudes = ParkingsContainer.getParkingsLongitudes();
            parkingLatitudes = ParkingsContainer.getParkingsLatitudes();
            parkingAvailabilities = ParkingsContainer.getParkingsAvailabilities();
        }

        //Variable que controla el login (podría estar guardada la sesión de alguna forma
        this.logged = false;

        loginBtn = (Button) findViewById(R.id.loginBtn);
        loginStatus = (TextView) findViewById(R.id.loginStatus);

        if (!logged) {
            logout();
        } else {
            if (userEmail != null) {
                login(userEmail);
            }
        }

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (logged) {
                    logout();
                } else {
                    Intent i = new Intent(context, LoginActivity.class);
                    startActivity(i);
                }
            }
        });

        setUpMapIfNeeded();

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                LatLng position = marker.getPosition();

                if (marker.getTitle().equals("Avaliable parking")) {
                    Parking parking = ParkingsContainer.getParking(position);
                    marker.setSnippet("Avaliable spaces: " + parking.getAvailable() + "\n" +
                                    "Parking cost: " + parking.getPricePerMinute() + "€/min"
                            /*+"\n"+ "Distance: "+parking.getDistance()*/);
                    marker.showInfoWindow();
                } else if (marker.getTitle().equals("Full parking")) {
                    marker.showInfoWindow();
                } else {
                    marker.setSnippet(streetPosition.toString());
                    marker.showInfoWindow();
                }

                return true;
            }
        });

        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            public void onInfoWindowClick(Marker marker) {
            LatLng position = marker.getPosition();

            marker.hideInfoWindow();

            if(marker.getTitle().equals("Reference")){
                return;
            }

            final Parking parking = ParkingsContainer.getParking(position);

            final Dialog datos_parking=new Dialog(context);
            datos_parking.setContentView(R.layout.parking_info_layout);
            datos_parking.setTitle("Car Park information");

            TextView parking_id = (TextView) datos_parking.findViewById(R.id.parkingID);
            parking_id.setText("ID: PARKING_"+parking.getId());

            TextView parking_distance = (TextView) datos_parking.findViewById(R.id.parkingDistance);

            int metros = (int) Float.parseFloat(parking.getDistance());
            if(metros<1000) {
                parking_distance.setText("Distance: " + metros +" meters");
            }else{
                int kilometros = metros/1000;
                parking_distance.setText("Distance: " + kilometros +" km");
            }

            TextView price = (TextView) datos_parking.findViewById(R.id.parkingPrice);
            price.setText("Price/minute: "+parking.getPricePerMinute() +"$");

            TextView space = (TextView) datos_parking.findViewById(R.id.parkingSpace);
            space.setText("Capacity: "+parking.getCapacity() +" slots");

            TextView avaliable = (TextView) datos_parking.findViewById(R.id.parkingAvaliable);
            avaliable.setText("Avaliable: "+parking.getAvailable() +" slots");

            TextView unavaliable = (TextView) datos_parking.findViewById(R.id.parkingUnavaliable);
            unavaliable.setText("Unavaliable: "+(Integer.parseInt(parking.getCapacity()) - Integer.parseInt(parking.getAvailable())) +" slots");

            //Como es redundante, esto no se muestra. Para mostrarlo, eliminar lo siguiente:
            unavaliable.setVisibility(View.GONE);

            Button book = (Button) datos_parking.findViewById(R.id.dialogButtonOK);

            if (!marker.getTitle().equals("Avaliable parking")) {
                book.setEnabled(false);
                TextView title = (TextView) datos_parking.findViewById(R.id.titleParking);
                title.setText("Full Parking Details");
            }

            book.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    datos_parking.cancel();

                    final Dialog dialog = new Dialog(context);
                    dialog.setContentView(R.layout.booking_layout);
                    dialog.setTitle("Booking car place");

                    String[] tipos_coche = new String[]{"SUV", "crossover", "compact", "electric car", "sports car", "pickup truck"};

                    final Spinner tipo_coche = (Spinner) dialog.findViewById(R.id.carType);
                    ArrayAdapter<String> adapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item, tipos_coche);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    tipo_coche.setAdapter(adapter);

                    String[] tipos_pago = new String[]{"Cash", "Visa", "Mastercard"};

                    final Spinner tipo_pago = (Spinner) dialog.findViewById(R.id.pmtType);
                    ArrayAdapter<String> adapter2 = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item, tipos_pago);
                    adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    tipo_pago.setAdapter(adapter2);

                    final EditText carNumber = (EditText) dialog.findViewById(R.id.carNumber);
                    final EditText carMake = (EditText) dialog.findViewById(R.id.carMake);
                    final EditText carColor = (EditText) dialog.findViewById(R.id.carColor);

                    final EditText startTime = (EditText) dialog.findViewById(R.id.time);
                    final EditText endTime = (EditText) dialog.findViewById(R.id.time2);

                    startTime.setHint("dd-mm-yyyy hh:mm");
                    endTime.setHint("dd-mm-yyyy hh:mm");

                    Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
                    // if button is clicked, close the custom dialog
                    dialogButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                        String c_number = carNumber.getText().toString();
                        String c_make = carMake.getText().toString();
                        String c_color = carColor.getText().toString();
                        String s_time = startTime.getText().toString();
                        String e_time = endTime.getText().toString();
                        String t_coche = tipo_coche.getSelectedItem().toString();
                        String t_pago = tipo_pago.getSelectedItem().toString();

                        if (!c_make.isEmpty() && !c_color.isEmpty() && !s_time.isEmpty() && !e_time.isEmpty()) {
                            String regex = "^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$";
                            String regex2 = "^(19|20)\\d\\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$";
                            if (!logged) {
                                Intent i = new Intent(context, LoginActivity.class);
                                startActivity(i);
                            } else {

                                s_time = s_time.trim().replaceAll("\\s+", " ");
                                e_time = e_time.trim().replaceAll("\\s+", " ");

                                String aux[] = s_time.split(" ");
                                String aux2[] = e_time.split(" ");

                                if (aux.length == 2 && aux2.length == 2) {
                                    String aux3[] = aux[0].split("-");
                                    String aux4[] = aux2[0].split("-");
                                    if (aux3.length == 3 && aux4.length == 3) {
                                        if (aux[1].matches(regex) && aux2[1].matches(regex)) {
                                            s_time = aux3[2] + "-" + aux3[1] + "-" + aux3[0] + "T" + aux[1] + ":00";
                                            e_time = aux4[2] + "-" + aux4[1] + "-" + aux4[0] + "T" + aux2[1] + ":00";
                                            if (s_time.split("T")[0].matches(regex2) && e_time.split("T")[0].matches(regex2)) {
                                                if(fecha_mayor(s_time,e_time)) {
                                                    ReserveTask plt = new ReserveTask(context, parking, userEmail, userPass, c_number, c_make, c_color, s_time, e_time, t_coche, t_pago);
                                                    plt.execute(); //Reserve - background
                                                    dialog.dismiss();
                                                }else{
                                                    Toast.makeText(context, "The start date must be before the end date!", Toast.LENGTH_SHORT).show();
                                                }
                                            } else {
                                                Toast.makeText(context, "Invalid time expression!", Toast.LENGTH_SHORT).show();
                                            }
                                        } else {
                                            Toast.makeText(context, "Invalid time expression!", Toast.LENGTH_SHORT).show();
                                        }
                                    } else {
                                        Toast.makeText(context, "Invalid time expression!", Toast.LENGTH_SHORT).show();
                                    }
                                } else {
                                    Toast.makeText(context, "Invalid time expression!", Toast.LENGTH_SHORT).show();
                                }
                            }
                        } else {
                            Toast.makeText(context, "All options are mandatory!", Toast.LENGTH_SHORT).show();
                        }
                        }
                    });
                    Button dialogButton2 = (Button) dialog.findViewById(R.id.btnCancel);
                    dialogButton2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.cancel();
                        }
                    });

                    dialog.show();
                }
            });

            Button cancel = (Button) datos_parking.findViewById(R.id.buttonCancel);
            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    datos_parking.cancel();
                }
            });

            datos_parking.show();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

    }

    /**
     * Sets up the map if it is possible to do so (i.e., the Google Play services APK is correctly
     * installed) and the map has not already been instantiated.. This will ensure that we only ever
     * call {@link #setUpMap()} once when {@link #mMap} is not null.
     * <p/>
     * If it isn't installed {@link SupportMapFragment} (and
     * {@link com.google.android.gms.maps.MapView MapView}) will show a prompt for the userEmail to
     * install/update the Google Play services APK on their device.
     * <p/>
     * A userEmail can return to this FragmentActivity after following the prompt and correctly
     * installing/updating/enabling the Google Play services. Since the FragmentActivity may not
     * have been completely destroyed during this process (it is likely that it would only be
     * stopped or paused), {@link #onCreate(Bundle)} may not be called again so we should call this
     * method in {@link #onResume()} to guarantee that it will be called.
     */
    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                setUpMap();
            }
        }
    }

    /**
     * This is where we can add markers or lines, add listeners or move the camera. In this case, we
     * just add a marker near Africa.
     * <p/>
     * This should only be called once and when we are sure that {@link #mMap} is not null.
     */
    private void setUpMap() {
        List<Marker> markersList = new ArrayList<>();
        if (streetPosition != null) {
            Marker streetMarker = mMap.addMarker(new MarkerOptions().position(streetPosition).title("Reference"));
            markersList.add(streetMarker);
        }
        if (parkingLatitudes != null && parkingLongitudes != null && parkingAvailabilities != null) {
            for (int i = 0; i < parkingLatitudes.length; i++) {
                Double currentParkingLatitude = Double.valueOf(parkingLatitudes[i]);
                Double currentParkingLongitude = Double.valueOf(parkingLongitudes[i]);
                LatLng currentParkingPosition = new LatLng(currentParkingLatitude, currentParkingLongitude);
                if (parkingAvailabilities[i].equals("yes")) {
                    Marker parkingMarker = mMap.addMarker(new MarkerOptions().position(currentParkingPosition).title("Avaliable parking")
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.ps)));
                    markersList.add(parkingMarker);
                } else {
                    Marker parkingMarker = mMap.addMarker(new MarkerOptions().position(currentParkingPosition).title("Full parking")
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.nps)));
                    markersList.add(parkingMarker);
                }
            }
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            for (Marker m : markersList) {
                builder.include(m.getPosition());
            }
            LatLngBounds bounds = builder.build();
            final CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 50);
            mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                @Override
                public void onMapLoaded() {
                    mMap.animateCamera(cu);
                }
            });
        }

        // Marker bubble adapter
        mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

            @Override
            public View getInfoWindow(Marker arg0) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {

                Context context = getApplicationContext();

                LinearLayout info = new LinearLayout(context);
                info.setOrientation(LinearLayout.VERTICAL);

                TextView title = new TextView(context);
                title.setTextColor(Color.BLACK);
                title.setGravity(Gravity.CENTER);
                title.setTypeface(null, Typeface.BOLD);
                title.setText(marker.getTitle());

                TextView snippet = new TextView(context);
                snippet.setTextColor(Color.GRAY);
                snippet.setText(marker.getSnippet());

                /*Button bt = new Button(context);
                bt.setText("Reserve");*/

                info.addView(title);
                info.addView(snippet);
                //info.addView(bt);

                return info;
            }
        });
    }

    public void onSearch(View view) {
        mMap.clear();
        EditText locationSearch = (EditText) findViewById(R.id.editText);
        String location = locationSearch.getText().toString();
        if(location.isEmpty()) {
            Toast.makeText(MapsActivity.this, "You must include a text before searching!", Toast.LENGTH_SHORT).show();
        }else {
            Geocoder coder = new Geocoder(this);
            try {
                ArrayList<Address> addresses = (ArrayList<Address>) coder.getFromLocationName(location, 50);
                Double latitude = addresses.get(0).getLatitude();
                Double longitude = addresses.get(0).getLongitude();
                streetPosition = new LatLng(latitude, longitude);
                ParkingsContainer.setStreetPosition(streetPosition);

                ParkLoadingTask plt = new ParkLoadingTask(this);
                plt.execute(); //in background

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private boolean valid_card(String card){
        try {
            String reverse = new StringBuilder().append(card).reverse().toString();
            int[] array = new int[card.length()];
            for (int i = 0; i < array.length; i++) {
                array[i] = Integer.parseInt("" + reverse.charAt(i));
            }
            int sum = 0;
            for (int i = 0; i < array.length; i++) {
                if (i % 2 == 1) {
                    array[i] *= 2;
                    if (array[i] > 9) {
                        array[i] -= 9;
                    }
                }
                sum += array[i];
            }
            return (sum % 10 == 0) && card.length()>=13 && card.length() <= 19;
        }catch(Exception e){
            return false;
        }
    }

    private boolean valid_type(String card,String type){
        if(type.equals("Visa")){
            return card.matches("^4[0-9]{12}(?:[0-9]{3})?$");
        }else if(type.equals("Mastercard")){
            return card.matches("^(?:5[1-5][0-9]{2}|222[1-9]|22[3-9][0-9]|2[3-6][0-9]{2}|27[01][0-9]|2720)[0-9]{12}$");
        }else if(type.equals("American Express")){
            return card.matches("^3[47][0-9]{13}$");
        }else{
            return true;
        }
    }

    private boolean valid_name(String name){
        return !name.isEmpty();
    }

    private boolean valid_cvv(String cvv){
        if(cvv.length()==3){
            try{
                Integer.parseInt(cvv);
                return true;
            }catch (Exception e){
                return false;
            }
        }else{
            return false;
        }
    }

    private boolean valid_exp(String exp){
        return exp.matches("^(0[1-9]|1[0-2])/[0-9]{2}$");
    }

    private boolean fecha_mayor(String f_inicial,String f_final){
        String fecha_i = f_inicial.replaceAll("T", ":").replaceAll("-", ":");
        String v[] = fecha_i.split(":");
        String fecha_f = f_final.replaceAll("T", ":").replaceAll("-", ":");
        String v2[] = fecha_f.split(":");

        int ano1 = Integer.parseInt(v[0]);
        int ano2 = Integer.parseInt(v2[0]);

        if(ano2<ano1){
            return false;
        }else if(ano2==ano1){
            int mes1 = Integer.parseInt(v[1]);
            int mes2 = Integer.parseInt(v2[1]);
            if(mes2<mes1) {
                return false;
            }else if(mes2==mes1){
                int dia1 = Integer.parseInt(v[2]);
                int dia2 = Integer.parseInt(v2[2]);
                if(dia2<dia1) {
                    return false;
                }else if(dia2==dia1) {
                    int h1 = Integer.parseInt(v[3]);
                    int h2 = Integer.parseInt(v2[3]);
                    if(h2<h1) {
                        return false;
                    }else if(h2==h1){
                        int m1 = Integer.parseInt(v[4]);
                        int m2 = Integer.parseInt(v2[4]);
                        if(m2<=m1) {
                            return false;
                        }
                    }
                }
            }
        }
        return true;
    }
}
