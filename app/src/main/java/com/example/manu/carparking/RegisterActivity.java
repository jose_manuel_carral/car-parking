package com.example.manu.carparking;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Map;

public class RegisterActivity extends ActionBarActivity {

    private Context context = this;

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) { //message received when LoginTask has finished
            String info = intent.getExtras().getString("info");
            String email = intent.getExtras().getString("userEmail");
            String pass = intent.getExtras().getString("userPass");
            if (info.equals("OK")){
                Toast.makeText(context, "Registered!", Toast.LENGTH_SHORT).show();
                //Mensaje al mapa de que el usuario se ha loggeado
                Context map_ctx = getApplicationContext();
                Intent in = new Intent("loggedIN").putExtra("userEmail",email).putExtra("userPass",pass);
                map_ctx.sendBroadcast(in);
                finish();
            }
            else if (info.contains("html")){
                Toast.makeText(context,"Error connecting to server", Toast.LENGTH_SHORT).show();
            }
            else {
                Toast.makeText(context, info, Toast.LENGTH_SHORT).show();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        IntentFilter intentFilter = new IntentFilter("registerIntent");
        registerReceiver(receiver, intentFilter);

        final EditText e_nombre = (EditText) findViewById(R.id.eNombre);
        final EditText e_apellidos = (EditText) findViewById(R.id.eApellidos);
        final EditText e_dni = (EditText) findViewById(R.id.eDNI);
        final EditText e_profesion = (EditText) findViewById(R.id.eProfesion);
        final EditText e_fecha = (EditText) findViewById(R.id.eFecha);
        e_fecha.setHint("year-month-day");
        final EditText e_email = (EditText) findViewById(R.id.eEmail);
        final EditText e_telefono = (EditText) findViewById(R.id.eTelefono);
        final EditText e_nacionalidad = (EditText) findViewById(R.id.eNacionalidad);
        final EditText e_passwd = (EditText) findViewById(R.id.ePasswd);

        Button registrar = (Button) findViewById(R.id.bRegistrar);
        Button cancelar = (Button) findViewById(R.id.bCancelar);

        cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        registrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nombre = e_nombre.getText().toString();
                String apellidos = e_apellidos.getText().toString();
                String dni = e_dni.getText().toString();
                String profesion = e_profesion.getText().toString();
                String fecha = e_fecha.getText().toString();
                String email = e_email.getText().toString();
                String telefono = e_telefono.getText().toString();
                String nacionalidad = e_nacionalidad.getText().toString();
                String password = e_passwd.getText().toString();

                if(!nombre.isEmpty() && !apellidos.isEmpty() && !dni.isEmpty() && !fecha.isEmpty()
                        && !email.isEmpty() && !telefono.isEmpty() && !nacionalidad.isEmpty() && !password.isEmpty()){

                    RegisterTask plt = new RegisterTask(context,nombre,apellidos,dni,profesion,fecha,email,telefono,nacionalidad,password);
                    plt.execute(); //Register - background

                }else{
                    Toast.makeText(context,"You must provide all the mandatory information!",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
