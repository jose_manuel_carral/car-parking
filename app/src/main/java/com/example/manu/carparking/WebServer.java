package com.example.manu.carparking;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * Created by manu on 30/10/2016.
 */
public class WebServer {
    private static String serverURL = "http://carparkapp.jelasticlw.com.br";

    public String register(String firstName, String lastName, String id, String profession, String dob, String phone, String email, String pass, String nationality) {
        String urlRequest = serverURL + "/webapi/account?firstName=" + firstName + "&lastName=" + lastName + "&personalIdType=DNI&personalId=" + id + "&profession=" + profession + "&dob=" + dob + "&phone=" + phone + "&email=" + email + "&nationality=" + nationality + "&password=" + pass;

        return get(urlRequest);
    }

    public String login(String email, String pass) {
        String urlRequest = serverURL + "/webapi/account/login?email=" + email + "&password=" + pass;

        return get(urlRequest);
    }

    public String getParkingsByPoint(String lat, String lon) {

        String urlRequest = serverURL + "/webapi/carpark?lat=" + lat + "&long=" + lon;

        return get(urlRequest);

    }

    public String booking(String email, String pass, String parkId, String cost, String number, String make, String color, String carType, String paymentType, String startDate, String endDate) {
        String urlRequest = serverURL + "/webapi/booking?username=" + email + "&password=" + pass + "&carParkId=" + parkId + "&bookingCost=" + cost + "&carRegNumber=" + number + "&carMake=" + make + "&carColor=" + color + "&carType=" + carType + "&paymentType=" + paymentType + "&startDateTime=" + startDate + "&endDateTime=" + endDate;

        return get(urlRequest);
    }

    public String payment(String email, String pass, String card_number, String price) {
        String urlRequest = serverURL + "/webapi/payment?username=" + email + "&password=" + pass + "&cardNumber=" + card_number + "&amount=" + price;
        return get(urlRequest);
    }

    private String get(String url) {
        try {
            HttpParams my_httpParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(my_httpParams, 5000);
            HttpResponse response = new DefaultHttpClient(my_httpParams).execute(new HttpGet(url));
            InputStream inputStreamResponse = response.getEntity().getContent();
            return inputStreamStringConverter(inputStreamResponse);

        } catch (Exception e) {
            e.printStackTrace();
            return e.getMessage();
        }
    }

    private String inputStreamStringConverter(InputStream is) {
        BufferedReader br = null;
        StringBuilder sb = new StringBuilder();
        String line;
        try {
            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return sb.toString();
    }

}
