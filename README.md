# CAR PARKING SYSTEM #

Este repositorio incluye una aplicación Android para la gestión de plazas de aparcamiento.
Esta aplicación se corresponde con el proyecto de la asignatura de Profundización en Ingeniería de Software del Máster en Ingeniería Informática de la Universidad Politécnica de Madrid.

### Información ###

* Version 3

### Equipo de desarrollo ###

* Alfonso Fernandez
* Ángel Soler
* José Manuel Carral
* Ivan Ribakovs
* Manuel Valbuena

### Servidor ###

La aplicación móvil depende de un servidor con una interfaz estándar basada en servicios web. Este servidor será necesario para el correcto despliegue de la aplicación.

### Instrucciones para el uso de GIT ###

Clonar repositorio
------------------
* `$ git clone https://jose_manuel_carral@bitbucket.org/jose_manuel_carral/car-parking.git`

Importación
------------------
* El proyecto clonado del repositorio deberá ser importado con Android Studio

Realizar cambios
------------------
* Se utilizarán las herramientas de integración con GIT de Android Studio

Dependencias
------------------

Este repositorio contiene una aplicación cliente de Android que requiere la instalación y el despliegue de un servidor. 

El código del servidor se encuenta en el siguiente repositorio de Bitbucket:

* `https://bitbucket.org/AnSoRu/carparkapp`

Una vez desplegado el servidor, se deberá modificar la clase WebServer.java actualizando la variable `serverURL` con la dirección URL en la que se ejecute dicho servidor.