package com.example.manu.carparking;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

/**
 * Created by manu on 13/11/2016.
 */

public class LoginTask extends AsyncTask<Void, Integer, String> {
    Context context;
    String email;
    String pass;
    ProgressDialog loginDialog;

    public LoginTask(Context c, String e, String p) {
        context = c;
        email = e;
        pass = p;

        loginDialog = new ProgressDialog(context);
        loginDialog.setMessage("Trying login");
        loginDialog.setIndeterminate(true);
        loginDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        loginDialog.setCancelable(true);
    }

    @Override
    protected void onPreExecute() {
        loginDialog.show();
    }

    @Override
    protected void onProgressUpdate(Integer... progress) {
        loginDialog.setIndeterminate(false);
        loginDialog.setMax(100);
        loginDialog.setProgress(progress[0]);
    }

    @Override
    protected String doInBackground(Void... params) {
        return new WebServer().login(email, pass);
    }

    @Override
    protected void onPostExecute(String info) {
        loginDialog.dismiss();
        Intent intent = new Intent("loginChecked").putExtra("info",info).putExtra("userEmail",email).putExtra("userPass",pass);
        context.sendBroadcast(intent); //message of finalisation to LoginActivity
    }
}
