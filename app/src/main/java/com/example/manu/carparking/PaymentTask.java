package com.example.manu.carparking;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

/**
 * Created by manu on 19/12/2016.
 */

public class PaymentTask extends AsyncTask<Void, Integer, String> {
    private Context context;
    private ProgressDialog dialog;

    private String email;
    private String card_number;
    private String card_name;
    private String card_cvv;
    private String card_date;
    private String price;
    private String password;

    public PaymentTask(Context c, String email,String password, String card_number, String price,String name,String cvv,String exp) {
        context = c;

        dialog = new ProgressDialog(context);
        dialog.setMessage("Requesting payment...");
        dialog.setIndeterminate(true);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setCancelable(true);

        this.email = email;
        this.card_number = card_number;
        this.price = price;
        this.password = password;

        card_name=name;
        card_cvv=cvv;
        card_date=exp;
    }

    @Override
    protected void onPreExecute() {
        dialog.show();
    }

    @Override
    protected void onProgressUpdate(Integer... progress) {
        dialog.setIndeterminate(false);
        dialog.setMax(100);
        dialog.setProgress(progress[0]);
    }

    @Override
    protected String doInBackground(Void... params) {
        return new WebServer().payment(email, password, card_number, price);
    }

    @Override
    protected void onPostExecute(String info) {
        dialog.dismiss();
        Intent intent = new Intent("paymentIntent").putExtra("info", info);
        context.sendBroadcast(intent);
    }
}
