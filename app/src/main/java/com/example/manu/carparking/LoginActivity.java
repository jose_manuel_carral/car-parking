package com.example.manu.carparking;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;


public class LoginActivity extends FragmentActivity {

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) { //message received when LoginTask has finished
            String info = intent.getExtras().getString("info");
            String email = intent.getExtras().getString("userEmail");
            String pass = intent.getExtras().getString("userPass");
            if (info.equals("OK")) {
                Toast.makeText(LoginActivity.this, "Logged in successfully", Toast.LENGTH_SHORT).show();
                //Mensaje al mapa de que el usuario se ha loggeado
                Context map_ctx = getApplicationContext();
                Intent in = new Intent("loggedIN").putExtra("userEmail",email).putExtra("userPass",pass);
                map_ctx.sendBroadcast(in);
                finish();
            } else if (info.contains("html")) {
                Toast.makeText(LoginActivity.this, "Error connecting to server", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(LoginActivity.this, info, Toast.LENGTH_SHORT).show();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        IntentFilter intentFilter = new IntentFilter("loginChecked");
        registerReceiver(receiver, intentFilter);
    }


    public void login(View view) {
        EditText emailEditText = (EditText) findViewById(R.id.email);
        EditText passEditText = (EditText) findViewById(R.id.pass);
        String email = emailEditText.getText().toString();
        String pass = passEditText.getText().toString();


        if (!email.isEmpty() && !pass.isEmpty()) {

            LoginTask plt = new LoginTask(this, email, pass);
            plt.execute(); //in background

        } else {
            Toast.makeText(this, "You must provide all the mandatory information!", Toast.LENGTH_SHORT).show();
        }

    }

    public void register(View view) {

        Intent iinent = new Intent(LoginActivity.this, RegisterActivity.class);
        startActivity(iinent);
        finish();

    }

    public void cancel(View view) {
        finish();
    }

}
