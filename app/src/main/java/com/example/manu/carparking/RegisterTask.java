package com.example.manu.carparking;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

/**
 * Created by jose on 16/11/2016.
 */

public class RegisterTask extends AsyncTask<Void, Integer, String> {
    private Context context;
    private ProgressDialog dialog;

    private String nombre;
    private String apellidos;
    private String dni;
    private String profesion;
    private String fecha;
    private String email;
    private String telefono;
    private String nacionalidad;
    private String password;

    public RegisterTask(Context c, String nombre, String apellidos,String dni, String profesion,String fecha, String email, String telefono,String nacionalidad,String password) {
        context = c;

        dialog = new ProgressDialog(context);
        dialog.setMessage("Registering ...");
        dialog.setIndeterminate(true);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setCancelable(true);

        this.nombre=nombre;
        this.apellidos=apellidos;
        this.dni=dni;
        this.profesion=profesion;
        this.fecha=fecha;
        this.email=email;
        this.telefono=telefono;
        this.nacionalidad=nacionalidad;
        this.password=password;
    }

    @Override
    protected void onPreExecute() {
        dialog.show();
    }

    @Override
    protected void onProgressUpdate(Integer... progress) {
        dialog.setIndeterminate(false);
        dialog.setMax(100);
        dialog.setProgress(progress[0]);
    }

    @Override
    protected String doInBackground(Void... params) {
        return new WebServer().register(nombre,apellidos,dni,profesion,fecha,telefono,email,password,nacionalidad);
    }

    @Override
    protected void onPostExecute(String info) {
        dialog.dismiss();
        Intent intent = new Intent("registerIntent").putExtra("info",info).putExtra("userEmail",email).putExtra("userPass",password);
        context.sendBroadcast(intent);
    }
}
