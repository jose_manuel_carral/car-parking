package com.example.manu.carparking;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by manu on 31/10/2016.
 */
public class ParkingsContainer {
    private static LatLng streetPosition;
    private static ArrayList<Parking> parkings = new ArrayList<>();


    public static void setStreetPosition(LatLng sp) {
        streetPosition = sp;
    }

    public static LatLng getStreetPosition() {
        return streetPosition;
    }

    public static Parking getParking(int i) {
        return parkings.get(i);
    }

    public static Parking getParking(LatLng position) {
        Parking parking = null;
        String latitude = String.valueOf(position.latitude);
        String longitude = String.valueOf(position.longitude);

        Iterator<Parking> it = parkings.iterator();

        while (it.hasNext()) {
            Parking p = it.next();
            if (p.getLongitude().equals(longitude) && p.getLatitude().equals(latitude)) {
                parking = p;
            }
        }
        return parking;
    }

    public static void addParking(Parking p) {
        parkings.add(p);
    }

    public static void clearParkings() {
        parkings.clear();
    }

    public static int parkingsNumber() {
        return parkings.size();
    }

    public static String[] getParkingsLatitudes() {
        String[] latitudes = new String[parkings.size()];
        for (int i = 0; i < parkings.size(); i++) {
            latitudes[i] = parkings.get(i).getLatitude();
        }
        return latitudes;
    }

    public static String[] getParkingsLongitudes() {
        String[] longitudes = new String[parkings.size()];
        for (int i = 0; i < parkings.size(); i++) {
            longitudes[i] = parkings.get(i).getLongitude();
        }
        return longitudes;
    }

    public static String[] getParkingsDistances() {
        String[] distances = new String[parkings.size()];
        for (int i = 0; i < parkings.size(); i++) {
            distances[i] = parkings.get(i).getDistance();
        }
        return distances;
    }

    public static String[] getParkingsPrices() {
        String[] capacities = new String[parkings.size()];
        for (int i = 0; i < parkings.size(); i++) {
            capacities[i] = parkings.get(i).getPricePerMinute();
        }
        return capacities;
    }

    public static String[] getParkingsCapacities() {
        String[] capacities = new String[parkings.size()];
        for (int i = 0; i < parkings.size(); i++) {
            capacities[i] = parkings.get(i).getCapacity();
        }
        return capacities;
    }

    public static String[] getParkingsAvailabilities() {
        String[] availabilities = new String[parkings.size()];
        for (int i = 0; i < parkings.size(); i++) {
            String available = parkings.get(i).getAvailable();
            if (available.equals("0")) {
                availabilities[i] = "no";
            } else {
                availabilities[i] = "yes";
            }
        }
        return availabilities;
    }
}
