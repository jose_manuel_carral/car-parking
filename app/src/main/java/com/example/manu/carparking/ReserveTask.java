package com.example.manu.carparking;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

/**
 * Created by jose on 16/11/2016.
 */

public class ReserveTask extends AsyncTask<Void, Integer, String> {
    private Context context;
    private Parking parking;
    private ProgressDialog dialog;

    private String email;
    private String password;
    private String c_number;
    private String c_make;
    private String c_color;
    private String s_time;
    private String e_time;
    private String t_coche;
    private String t_pago;
    private String totalPrice;

    public ReserveTask(Context c, Parking p, String email, String password, String c_number, String c_make, String c_color, String s_time, String e_time, String t_coche, String t_pago) {
        context = c;
        parking = p;

        dialog = new ProgressDialog(context);
        dialog.setMessage("Ordering car space...");
        dialog.setIndeterminate(true);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setCancelable(true);

        this.email = email;
        this.password = password;
        this.c_number = c_number;
        this.c_make = c_make;
        this.c_color = c_color;
        this.s_time = s_time;
        this.e_time = e_time;
        this.t_coche = t_coche;
        this.t_pago = t_pago;
    }

    @Override
    protected void onPreExecute() {
        dialog.show();
    }

    @Override
    protected void onProgressUpdate(Integer... progress) {
        dialog.setIndeterminate(false);
        dialog.setMax(100);
        dialog.setProgress(progress[0]);
    }

    @Override
    protected String doInBackground(Void... params) {

        Double totalPriceD;

        String fecha_i = s_time.replaceAll("T", ":").replaceAll("-", ":");

        String v[] = fecha_i.split(":");

        String fecha_f = e_time.replaceAll("T", ":").replaceAll("-", ":");

        String v2[] = fecha_f.split(":");

        int anos = Integer.parseInt(v2[0]) - Integer.parseInt(v[0]);
        int meses = Integer.parseInt(v2[1]) - Integer.parseInt(v[1]);
        int dias = Integer.parseInt(v2[2]) - Integer.parseInt(v[2]);
        int horas = Integer.parseInt(v2[3]) - Integer.parseInt(v[3]);
        int minutos = Integer.parseInt(v2[4]) - Integer.parseInt(v[4]);

        int min_t = anos * 365 * 24 * 60 + meses * 30 * 24 * 60 + dias * 24 * 60 + horas * 60 + minutos;

        totalPriceD = Double.parseDouble(parking.getPricePerMinute()) * min_t;

        totalPrice = Double.toString(totalPriceD);

        return new WebServer().booking(email, password, parking.getId(), totalPrice, c_number, c_make, c_color, t_coche, t_pago, s_time, e_time);

    }

    @Override
    protected void onPostExecute(String info) {
        dialog.dismiss();
        Intent intent = new Intent("reserveIntent").putExtra("info", info).putExtra("price", totalPrice).putExtra("type", t_pago);
        context.sendBroadcast(intent);
    }
}
