package com.example.manu.carparking;

/**
 * Created by manu on 31/10/2016.
 */
public class Parking {
    String id;
    String latitude;
    String longitude;
    String capacity;
    String available;
    String pricePerMinute;
    String distance;

    public Parking(String id, String lat, String lon, String capacity, String available, String pricePerMinute, String distance) {
        this.id = id;
        this.latitude = lat;
        this.longitude = lon;
        this.capacity = capacity;
        this.available = available;
        this.pricePerMinute = pricePerMinute;
        this.distance = distance;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getCapacity() {
        return capacity;
    }

    public void setCapacity(String capacity) {
        this.capacity = capacity;
    }

    public String getAvailable() {
        return available;
    }

    public void setAvailable(String available) {
        this.available = available;
    }

    public String getPricePerMinute() {
        return pricePerMinute;
    }

    public void setPricePerMinute(String pricePerMinute) {
        this.pricePerMinute = pricePerMinute;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }
}
